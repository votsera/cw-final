# CW-Final
#Функционал авторизации и регистрации
Реализован полностью. Доступен на страницах /login, /register соответственно.

#Функционал добавления новости.
Реализован полностью. Доступен на странице /create.

#Функционал Добавления комментария к новости
Реализован полностью. Доступен на странице самой новости.

#Функционал модерации новости.
Реализовано. Для указания даты публикации необходимо зайти в личный кабинет, отобразится список новостей к публикации. Под каждой новостью переход к настройке публикации.

#Сортировка по тегам и категориям.
Реализована не полностью. Доступна при нажатии на тег или категорию. При поиске по тегу выдается ошибка.

#Пагинация
Присутствует только на главной странице.

#Модерация комментариев.
Не реализовано.

#Функционал рейтинга.
Не реализовано. Попытки были, но время сильно ограничивает.

#Функционал просмотра пользователя
Реализован полностью, за исключением просмотра рейтинга. 