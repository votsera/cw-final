<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method News|null find($id, $lockMode = null, $lockVersion = null)
 * @method News|null findOneBy(array $criteria, array $orderBy = null)
 * @method News[]    findAll()
 * @method News[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NewsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, News::class);
    }

    /**
     * @return News[] Returns an array of News objects
     */
    public  function findAllApprovedNews(){
        $news = $this->findAll();
        $approved_news = [];
        $date = new \DateTime();
        foreach ($news as $n){
            if (!$n->getPublishDate() == null && $date->diff($n->getPublishDate())){
                $approved_news[] = $n;
            }
        }
        return $approved_news;
    }

    /**
     * @param int $id
     * @return News[] Returns an array of News objects
     */

    public function findByAuthor(int $id)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.author = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $id
     * @param TagRepository $tagRepository
     * @return News[] Returns an array of News objects
     */
    public function findByTag(
        int $id,
        TagRepository $tagRepository
    )
    {
        $news = $this->findAll();
        $tag_news = [];
        $tag = $tagRepository->find($id);
        $date = new \DateTime();
        dump($tag);
        foreach ($news as $n){
            if (!$n->getPublishDate() == null && $date->diff($n->getPublishDate())){
                if (!is_null($n->getTags())){
                    if (in_array($id, $n->getTags()->getKeys())){
                        dump($tag);
                        $tag_news[] = $n;
                    }
                }
            }
        }
        return $tag_news;
    }


    /**
     * @param int $id
     * @return News[] Returns an array of News objects
     */
    public function findByCategory(int $id)
    {
        return $this->createQueryBuilder('n')
            ->andWhere('n.category = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return News[] Returns an array of News objects
     */
    public function findAllNotPublished()
    {
        $news = $this->findAll();
        $not_published_news = [];
        foreach ($news as $n){
            if ($n->getPublishDate() === null)
                $not_published_news[] = $n;
        }
        return $not_published_news;
    }


}
