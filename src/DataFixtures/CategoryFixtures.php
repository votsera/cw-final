<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public const CATEGORY_ONE = 'Наука';
    public const CATEGORY_TWO = 'Политика';
    public const CATEGORY_THREE = 'Спорт';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $category_1 = new Category();
        $category_1
            ->setTitle(self::CATEGORY_ONE);
        $manager->persist($category_1);

        $category_2 = new Category();
        $category_2
            ->setTitle(self::CATEGORY_TWO);
        $manager->persist($category_2);

        $category_3 = new Category();
        $category_3
            ->setTitle(self::CATEGORY_THREE);
        $manager->persist($category_3);

        $manager->flush();

        $this->addReference(self::CATEGORY_ONE, $category_1);
        $this->addReference(self::CATEGORY_TWO, $category_2);
        $this->addReference(self::CATEGORY_THREE, $category_3);
    }
}