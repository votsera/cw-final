<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\News;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class NewsFixtures extends Fixture implements DependentFixtureInterface
{
    public const NEWS_ONE = 'Новость 1';
    public const NEWS_TWO = 'Новость 2';
    public const NEWS_THREE = 'Новость 3';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /** @var User $user1 */
        $user1 = $this->getReference(UserFixtures::USER_ONE);
        /** @var User $user2 */
        $user2 = $this->getReference(UserFixtures::USER_TWO);
        /** @var User $user3 */
        $user3 = $this->getReference(UserFixtures::USER_THREE);

        /** @var Category $category1 */
        $category1 = $this->getReference(CategoryFixtures::CATEGORY_ONE);
        /** @var Category $category2 */
        $category2 = $this->getReference(CategoryFixtures::CATEGORY_TWO);
        /** @var Category $category3 */
        $category3 = $this->getReference(CategoryFixtures::CATEGORY_THREE);

        $news_1 = new News();
        $news_1
            ->setTitle(self::NEWS_ONE)
            ->setAuthor($user1)
            ->setBody("qwertyuikjhgfds
            dvfdnjkf dhsjks  
             gfg
             d bgfd gf
             j fgj fj ghj fjgf
             ")
            ->setCategory($category1)
            ->setCreateDate(new \DateTime())
        ;
        $manager->persist($news_1);

        $news_2 = new News();
        $news_2
            ->setTitle(self::NEWS_TWO)
            ->setAuthor($user2)
            ->setBody("qwertyuikjhgfds
            dvfdnjkf dhsjks  
             gfg
             d bgfd gf
             j fgj fj ghj fjgf
             ")
            ->setCategory($category2)
            ->setCreateDate(new \DateTime())
        ;
        $manager->persist($news_2);


        $news_3 = new News();
        $news_3
            ->setTitle(self::NEWS_THREE)
            ->setAuthor($user3)
            ->setPublishDate(new \DateTime())
            ->setBody("qwertyuikjhgfds
            dvfdnjkf dhsjks  
             gfg
             d bgfd gf
             j fgj fj ghj fjgf
             ")
            ->setCategory($category3)
            ->setCreateDate(new \DateTime())
        ;
        $manager->persist($news_3);



        $manager->flush();

        $this->addReference(self::NEWS_ONE, $news_1);
        $this->addReference(self::NEWS_TWO, $news_2);
        $this->addReference(self::NEWS_THREE, $news_3);
    }


    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            CategoryFixtures::class,
        );
    }

}