<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public const USER_ONE = 'user1@gmail.com';
    public const USER_TWO = 'user2@gmail.com';
    public const USER_THREE = 'user3@gmail.com';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $admin = new User();
        $admin
            ->setUsername("admin")
            ->setEnabled(true)
            ->setEmail("admin@cw.com")
            ->setPlainPassword('qwerty')
            ->setRoles(['ROLE_ADMIN'])
        ;
        $manager->persist($admin);

        $user1 = new User();
        $user1
            ->setUsername(self::USER_ONE)
            ->setEnabled(true)
            ->setEmail(self::USER_ONE)
            ->setPlainPassword('qwerty')
            ->setRoles(['ROLE_USER'])
        ;
        $manager->persist($user1);

        $user2 = new User();
        $user2
            ->setUsername(self::USER_TWO)
            ->setEnabled(true)
            ->setEmail(self::USER_TWO)
            ->setPlainPassword('qwerty')
            ->setRoles(['ROLE_USER'])
        ;
        $manager->persist($user2);

        $user3 = new User();
        $user3
            ->setUsername(self::USER_THREE)
            ->setEnabled(true)
            ->setEmail(self::USER_THREE)
            ->setPlainPassword('qwerty')
            ->setRoles(['ROLE_USER'])
        ;
        $manager->persist($user3);

        $manager->flush();

        $this->addReference(self::USER_ONE, $user1);
        $this->addReference(self::USER_TWO, $user2);
        $this->addReference(self::USER_THREE, $user3);
    }
}