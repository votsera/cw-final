<?php


namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TagFixtures extends Fixture
{

    public const TAG_ONE = 'открытия';
    public const TAG_TWO = 'США';
    public const TAG_THREE = 'футбол';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $tag_1 = new Tag();
        $tag_1->setTitle(self::TAG_ONE);
        $manager->persist($tag_1);

        $tag_2 = new Tag();
        $tag_2->setTitle(self::TAG_TWO);
        $manager->persist($tag_2);

        $tag_3 = new Tag();
        $tag_3->setTitle(self::TAG_THREE);
        $manager->persist($tag_3);

        $manager->flush();

        $this->addReference(self::TAG_ONE, $tag_1);
        $this->addReference(self::TAG_TWO, $tag_2);
        $this->addReference(self::TAG_THREE, $tag_3);
    }
}