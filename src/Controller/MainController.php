<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\News;
use App\Entity\Rating;
use App\Repository\CategoryRepository;
use App\Repository\NewsRepository;
use App\Repository\TagRepository;
use phpDocumentor\Reflection\DocBlock\Tag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method("GET")
     *
     * @param NewsRepository $newsRepository
     * @param CategoryRepository $categoryRepository
     * @param Request $request
     * @param TagRepository $tagRepository
     * @return Response
     */
    public function indexAction(
        NewsRepository $newsRepository,
        CategoryRepository $categoryRepository,
        Request $request,
        TagRepository $tagRepository
    )
    {
        $news = $newsRepository->findAllApprovedNews();
        $paginator = $this->get('knp_paginator');
        $page = $request->query->getInt("page", 1);

        /** @var News[] $articlesResult */
        $newsResult = $paginator->paginate(
            $news,
            $page,
            $request->query->getInt("limit", 2)
        );
        return $this->render('site/main/index.html.twig', [
            'news' => $newsResult,
            'categories' => $categoryRepository->findAll(),
            'tags' => $tagRepository->findAll(),
        ]);
    }
}
