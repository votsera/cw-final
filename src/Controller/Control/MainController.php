<?php

namespace App\Controller\Control;


use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class MainController
 * @package App\Controller\Control
 * @Route("control")
 */
class MainController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render("control/main/index.html.twig");
    }

    /**
     * @Route("/list")
     */
    public function listAction()
    {
        return $this->render("control/main/list.html.twig");
    }

    /**
     * @Route("/add")
     */
    public function addAction()
    {
        return $this->render("control/main/add.html.twig");
    }
}