<?php

namespace App\Controller;

use App\Entity\News;
    use App\Entity\User;
    use App\Form\NewsType;
    use App\Repository\CategoryRepository;
    use App\Repository\NewsRepository;
    use App\Repository\TagRepository;
    use App\Repository\UserRepository;
    use Doctrine\Common\Collections\ArrayCollection;
    use Doctrine\Common\Persistence\ObjectManager;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Validator\Constraints\Date;

class UserController extends Controller
{

    /**
     * @Route("user/{id}/", name="show_user", requirements={"id"="\d+"})
     * @Method("GET")
     *
     * @param int $id
     * @param NewsRepository $newsRepository
     * @param CategoryRepository $categoryRepository
     * @param TagRepository $tagRepository
     * @param UserRepository $userRepository
     * @return Response
     */
    public function showUserAction(
        int $id,
        NewsRepository $newsRepository,
        CategoryRepository $categoryRepository,
        TagRepository $tagRepository,
        UserRepository $userRepository
)
    {
        return $this->render('site/main/show_user.html.twig', [
            'categories' => $categoryRepository->findAll(),
            'tags' => $tagRepository->findAll(),
            'news' => $newsRepository->findByAuthor($id),
            'user' => $userRepository->find($id)
        ]);
    }

}