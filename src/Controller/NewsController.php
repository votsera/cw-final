<?php

namespace App\Controller;


use App\Entity\Comment;
use App\Entity\News;
use App\Form\CommentType;
use App\Form\PublishDateType;
use App\Form\NewsType;
use App\Repository\CategoryRepository;
use App\Repository\NewsRepository;
use App\Repository\TagRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NewsController extends Controller
{

    /**
     *
     * @Route("/create", name="create_news")
     *
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param ObjectManager $manager
     * @param CategoryRepository $categoryRepository
     * @param TagRepository $tagRepository
     *
     * @return Response
     *
     */
    public function addNewsAction(
        Request $request,
        ObjectManager $manager,
        CategoryRepository $categoryRepository,
        TagRepository $tagRepository
    )
    {
        if ($this->getUser() === null){
            return $this->redirectToRoute('homepage');
        }
        $news = new News();
        $form = $this->createForm(NewsType::class, $news);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $news->setAuthor($this->getUser());
            $news->setCreateDate(new \DateTime());
            if (in_array('ROLE_ADMIN', $this->getUser()->getRoles())){
                $news->setPublishDate(new \DateTime());
            }
            $manager->persist($news);
            $manager->flush();

            return $this->redirectToRoute("homepage");
        }

        return $this->render('site/main/create_news.html.twig', [
            'form' => $form->createView(),
            'categories' => $categoryRepository->findAll(),
            'tags' => $tagRepository->findAll(),
        ]);
    }

    /**
     * @Route("profile", name="profile", requirements={"id"="\d+"})
     * @Method("GET")
     *
     * @param NewsRepository $newsRepository
     * @param CategoryRepository $categoryRepository
     * @param TagRepository $tagRepository
     * @return Response
     */
    public function approveNewsAction(
        NewsRepository $newsRepository,
        CategoryRepository $categoryRepository,
        TagRepository $tagRepository
    )
    {
        if ($this->getUser() === null){
            return $this->redirectToRoute('homepage');
        }
        if (in_array('ROLE_ADMIN', $this->getUser()->getRoles()))
        {
            return $this->render('site/main/profile.html.twig', [
                'categories' => $categoryRepository->findAll(),
                'tags' => $tagRepository->findAll(),
                'news' => $newsRepository->findAllNotPublished(),
                'user' => $this->getUser()
            ]);
        }

    }

    /**
     * @Route("news/{id}/approve", name="approve_news", requirements={"id"="\d+"})
     * @Method("GET")
     *
     * @param int $id
     * @param NewsRepository $newsRepository
     * @param CategoryRepository $categoryRepository
     * @param TagRepository $tagRepository
     * @param Request $request
     * @param ObjectManager $manager
     * @return Response
     */
    public function approveAction(
        int $id,
        NewsRepository $newsRepository,
        CategoryRepository $categoryRepository,
        TagRepository $tagRepository,
        Request $request,
        ObjectManager $manager)
    {
        if ($this->getUser() === null){
            return $this->redirectToRoute('homepage');
        }
        $news = $newsRepository->find($id);
        if (in_array('ROLE_ADMIN', $this->getUser()->getRoles()))
        {
            $form = $this->createForm(PublishDateType::class, $news);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $manager->persist($news);
                $manager->flush();
            }
        }

        return $this->render('site/main/approve.html.twig', [
            'form' => $form->createView(),
            'categories' => $categoryRepository->findAll(),
            'tags' => $tagRepository->findAll(),
            'news' => $news
        ]);
    }


    /**
     * @Route("news/{id}/", name="show_news", requirements={"id"="\d+"})
     * @Method("GET")
     *
     * @param int $id
     * @param NewsRepository $newsRepository
     * @param CategoryRepository $categoryRepository
     * @param TagRepository $tagRepository
     * @param Request $request
     * @param ObjectManager $manager
     * @return Response
     */
    public function showNewsAction(
        int $id,
        NewsRepository $newsRepository,
        CategoryRepository $categoryRepository,
        TagRepository $tagRepository,
        Request $request,
        ObjectManager $manager)
    {

        $news = $newsRepository->find($id);
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $comment->setAuthor($this->getUser());
            $comment->setNews($news);
            $comment->setCreatedAt(new \DateTime());
            $manager->persist($comment);
            $manager->flush();
        }
        if (!$this->getUser() === null) {
            if (in_array('ROLE_ADMIN', $this->getUser()->getRoles()))
            {
                $date_form = $this->createForm(PublishDateType::class, $news);
                $date_form->handleRequest($request);
                if ($date_form->isSubmitted() && $date_form->isValid()) {
                    $manager->persist($news);
                    $manager->flush();
                }

            }
        }


        return $this->render('site/main/show_news.html.twig', [
            'form' => $form->createView(),
            'categories' => $categoryRepository->findAll(),
            'tags' => $tagRepository->findAll(),
            'news' => $news
        ]);
    }


    /**
     * @Route("category/{id}/", name="category_news", requirements={"id"="\d+"})
     * @Method("GET")
     *
     * @param int $id
     * @param NewsRepository $newsRepository
     * @param CategoryRepository $categoryRepository
     * @param TagRepository $tagRepository
     * @return Response
     */
    public function newsByCategoryAction(
        int $id,
        NewsRepository $newsRepository,
        CategoryRepository $categoryRepository,
        TagRepository $tagRepository)
    {
        return $this->render('site/main/news.html.twig', [
            'categories' => $categoryRepository->findAll(),
            'tags' => $tagRepository->findAll(),
            'news' => $newsRepository->findByCategory($id),
        ]);
    }

    /**
     * @Route("tag/{id}/", name="tag_news", requirements={"id"="\d+"})
     * @Method("GET")
     *
     * @param int $id
     * @param NewsRepository $newsRepository
     * @param CategoryRepository $categoryRepository
     * @param TagRepository $tagRepository
     * @return Response
     */
    public function newsByTagAction(
        int $id,
        NewsRepository $newsRepository,
        CategoryRepository $categoryRepository,
        TagRepository $tagRepository)
    {
        return $this->render('site/main/news.html.twig', [
            'categories' => $categoryRepository->findAll(),
            'tags' => $tagRepository->findAll(),
            'news' => $newsRepository->findByTag($id, $tagRepository)
        ]);
    }
}