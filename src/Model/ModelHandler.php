<?php

namespace App\Model;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ModelHandler
{
    protected $manager;
    protected $container;

    /**
     * ModelHandler constructor.
     * @param ContainerInterface $container
     * @param ObjectManager $manager
     */
    public function __construct(ContainerInterface $container, ObjectManager $manager)
    {
        $this->container = $container;
        $this->manager = $manager;
    }
}