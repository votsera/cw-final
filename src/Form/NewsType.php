<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\News;
use App\Entity\Tag;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('POST')
            ->add('title', TextType::class, ['label' => 'Заголовок'])
            ->add('body', TextType::class, ['label' => 'Новость'])
            ->add('category', EntityType::class, [
                'label' => 'Категория',
                'class' => Category::class,
                'choice_label' => 'title',
                'required' => false,
                'placeholder' => 'Выберите категорию'
            ])
            ->add('tags', EntityType::class, [
                'label' => 'Тэги',
                'class' => Tag::class,
                'choice_label' => 'title',
                'required' => false,
                'multiple'  => true,
                'placeholder' => 'Выберите тэги'
            ])
            ->add('Добавить', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }
}
