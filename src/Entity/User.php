<?php

namespace App\Entity;;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;


    /**
     * @var News[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\News", mappedBy="author")
     */
    private $news;


    /**
     * @var Comment[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="author")
     */
    private $comments;


    /**
     * @var Rating[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Rating", mappedBy="who_rated")
     */
    private $rating;


    public function __construct()
    {
        parent::__construct();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param News[]|ArrayCollection $news
     * @return User
     */
    public function setNews($news)
    {
        $this->news = $news;
        return $this;
    }

    /**
     * @return News[]|ArrayCollection
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * @param Comment[]|ArrayCollection $comments
     * @return User
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @return Comment[]|ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param Rating[]|ArrayCollection $likes
     * @return User
     */
    public function setLikes($likes)
    {
        $this->likes = $likes;
        return $this;
    }

    /**
     * @return Rating[]|ArrayCollection
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param mixed $rating
     * @return User
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }
}